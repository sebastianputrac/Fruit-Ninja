using Core.Fruit;
using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Burst.CompilerServices;
using UnityEngine;

namespace Core.Player
{
	public class PlayerController : MonoBehaviour
	{
		public bool IsPause;

		public Action BombClicked;
		public Action<int> FruitClicked;

		private void Update()
		{
			if (!IsPause)
			{
				HandleInput();
			}
		}
		private void HandleInput()
		{
			if (Input.GetMouseButtonDown(0))
			{
				Vector2 mousePosition = new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y);
				RaycastHit2D hit = Physics2D.Raycast(mousePosition, Vector2.zero, 0f);

				if (hit.collider == null)
				{
					return;
				}

				CheckClickedObject(hit);
			}
		}
		private void CheckClickedObject(RaycastHit2D objectHit)
		{
			if (objectHit.transform.gameObject.GetComponent<FruitData>().Type == FruitType.Fruit)
			{
				FruitClicked?.Invoke(1);
				//Destroy(objectHit.transform.gameObject);
				objectHit.transform.gameObject.SetActive(false);
			}
			else if (objectHit.transform.gameObject.GetComponent<FruitData>().Type == FruitType.Bomb)
			{
				BombClicked?.Invoke();
				//Destroy(objectHit.transform.gameObject);
				objectHit.transform.gameObject.SetActive(false);
			}
		}
	}
}
