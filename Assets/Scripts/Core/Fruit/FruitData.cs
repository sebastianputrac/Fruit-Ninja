using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core.Fruit
{
	public class FruitData : MonoBehaviour
	{
		private Vector2 storedVelocity;
		public FruitType Type;

		public void StoreVelocityAndStop()
		{
			storedVelocity = GetComponent<Rigidbody2D>().velocity;
			GetComponent<Rigidbody2D>().velocity = Vector2.zero;
			GetComponent<Rigidbody2D>().gravityScale = 0;
		}
		public void ResumeVelocity()
		{
			GetComponent<Rigidbody2D>().velocity = storedVelocity;
			GetComponent<Rigidbody2D>().gravityScale = 1;
		}
		private void OnDisable()
		{
			this.transform.position = new Vector2(0, -6);

		}
	}
}
