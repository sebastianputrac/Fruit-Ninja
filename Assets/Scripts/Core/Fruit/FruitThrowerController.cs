using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;

namespace Core.Fruit
{
	public class FruitThrowerController : MonoBehaviour
	{
		[SerializeField] private GameObject[] fruitPrefabs;
		[SerializeField] private List<GameObject> pooledFruit;
		[SerializeField] private Transform fruitParent;

		private const float initialTime = 1.75f;
		private float timer = initialTime; //Buat variable constant untuk waktu

		private bool isPlay = false;
		public Action<int> FruitHitGround;

		private void Update()
		{
			if (isPlay)
			{
				ThrowTimer();
			}
		}
		private void OnTriggerEnter2D(Collider2D collision)
		{
			if (collision.gameObject.CompareTag("Fruit"))
			{
				FruitHitGround?.Invoke(1);
			}
			//Destroy(collision.gameObject);
			collision.gameObject.SetActive(false);	
		}
		public void StartThrower()
		{
			isPlay = true;
		}
		public void ResetThrower()
		{
			isPlay = false;
			timer = initialTime;

			for(int i = 0; i < fruitParent.childCount; i++)
			{
				Destroy(fruitParent.GetChild(i).gameObject);
				pooledFruit.Clear();
			}
		}
		public void StopFruits()
		{
			isPlay = false;
			for (int i = 0; i < fruitParent.childCount; i++)
			{
				fruitParent.GetChild(i).GetComponent<FruitData>().StoreVelocityAndStop();
			}
		}
		public void ResumeFruits()
		{
			isPlay = true;
			for (int i = 0; i < fruitParent.childCount; i++)
			{
				fruitParent.GetChild(i).GetComponent<FruitData>().ResumeVelocity();
			}
		}
		private void ThrowTimer()
		{
			timer -= Time.deltaTime;
			if (timer <= 0)
			{
				timer = initialTime;
				//ThrowFruit(UnityEngine.Random.Range(1,5));
				SpawnPooledFruit(UnityEngine.Random.Range(1, 5));
			}
		}
		/*private void ThrowFruit(int fruitAmount)
		{
			for (int i = 0; i < fruitAmount; i++) //Move to object pooling
			{
				Vector3 throwPosition = new Vector3(UnityEngine.Random.Range(-8, 8), -6, 0);

				Vector2 force = RandomnizeForce(throwPosition.x);

				int randomFruit = UnityEngine.Random.Range(0, fruitPrefabs.Length);
				GameObject newFruit = Instantiate(fruitPrefabs[randomFruit], throwPosition, this.transform.rotation, fruitParent);
				newFruit.GetComponent<Rigidbody2D>().AddForce(force);
			}
		}*/
		private void SpawnPooledFruit(int fruitAmount)
		{
			for (int i = 0; i < fruitAmount; i++)
			{
				Vector3 throwPosition = new Vector3(UnityEngine.Random.Range(-8, 8), -6, 0);
				Vector2 force = RandomnizeForce(throwPosition.x);

				GameObject newObject = FindInactiveFruit();

				if (newObject != null)
				{
					ResetInactiveFruit(newObject, throwPosition, force);
				}
				else
				{
					InstantiateNewFruit(throwPosition, force);
				}
			}
		}
		private Vector2 RandomnizeForce(float throwPositionX)
		{
			float forceX = 0;
			if (throwPositionX < 0)
			{
				forceX = UnityEngine.Random.Range(150, 200);
			}
			else
			{
				forceX = UnityEngine.Random.Range(-150, -200);
			}

			float forceY = UnityEngine.Random.Range(550, 700);
			Vector2 force = new Vector2(forceX, forceY);

			return force;
		}
		private GameObject FindInactiveFruit()
		{
			for (int i = 0; i < pooledFruit.Count; i++)
			{
				if (pooledFruit[i].activeInHierarchy == false)
				{
					return pooledFruit[i];
				}
			}
			return null;
		}
		private void ResetInactiveFruit(GameObject inactiveFruit, Vector3 throwPosition, Vector2 force)
		{
			inactiveFruit.SetActive(true);
			inactiveFruit.transform.position = throwPosition;
			inactiveFruit.GetComponent<Rigidbody2D>().AddForce(force);
		}
		private void InstantiateNewFruit(Vector3 throwPosition, Vector2 force)
		{
			int randomFruit = UnityEngine.Random.Range(0, fruitPrefabs.Length);
			GameObject newFruit = Instantiate(fruitPrefabs[randomFruit], throwPosition, this.transform.rotation, fruitParent);
			newFruit.GetComponent<Rigidbody2D>().AddForce(force);
			pooledFruit.Add(newFruit);
		}
	}
}
