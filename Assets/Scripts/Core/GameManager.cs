using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Core.Player;
using Core.Fruit;
using Core.Menu;
namespace Core
{
	public class GameManager : MonoBehaviour
	{
		[SerializeField] private PlayerController player;
		[SerializeField] private FruitThrowerController fruitThrower;
		[SerializeField] private StartMenuController startMenu;
		[SerializeField] private PauseMenuController pauseMenu;
		[SerializeField] private GameOverMenuController gameOverMenu;

		private bool onMenu = true; 
		private int points;
		private int health;
		private const int initialHealth = 3;
		private const int initialPoints = 0;

		public Action<int> PointGained;
		public Action<int> HealthLost;
		public Action BombExploded;
		public Action GameRestarted;

		private void Awake()
		{
			player.BombClicked += BombExplode;
			player.FruitClicked += AddPoints;

			fruitThrower.FruitHitGround += RemoveHealth;

			startMenu.StartPressed += StartGame;
			startMenu.ExitApplicationPressed += ExitApplication;

			pauseMenu.ResumePressed += Resume;
			pauseMenu.RestartPressed += StartGame;
			pauseMenu.backToMenuPressed += BackToMenu;

			gameOverMenu.RestartPressed += StartGame;
			gameOverMenu.backToMenuPressed += BackToMenu;
		}
		private void Update()
		{
			if (Input.GetKeyDown(KeyCode.P) && !onMenu)
			{
				Pause();
			}	
		}

		private void StartGame()
		{
			pauseMenu.Deactivate();
			startMenu.Deactivate();
			gameOverMenu.Deactivate();
			fruitThrower.StartThrower();

			onMenu = false;
			player.IsPause = false;

			points = initialPoints;
			health = initialHealth;

			GameRestarted?.Invoke();
		}
		private void Pause()
		{
			pauseMenu.Activate();
			player.IsPause = true;
			fruitThrower.StopFruits();
		}
		private void Resume()
		{
			pauseMenu.Deactivate();
			player.IsPause = false;
			fruitThrower.ResumeFruits();
		}
		private void GameOver()
		{
			gameOverMenu.Activate();
			fruitThrower.ResetThrower();
		}
		private void BackToMenu()
		{
			pauseMenu.Deactivate();
			gameOverMenu.Deactivate();
			startMenu.Activate();
			fruitThrower.ResetThrower();

			onMenu = true;
		}
		private void ExitApplication()
		{
			Application.Quit();
		}
		private void AddPoints(int points)
		{
			this.points += points;
			PointGained?.Invoke(this.points);
		}
		private void RemoveHealth(int damage)
		{
			health -= damage;
			HealthLost?.Invoke(health);

			if (health <= 0) //Condition
			{
				GameOver();
			}
		}
		private void BombExplode()
		{
			RemoveHealth(3);
			BombExploded?.Invoke();

			GameOver();
		}
		private void OnDestroy()
		{
			player.BombClicked -= BombExplode;
			player.FruitClicked -= AddPoints;

			fruitThrower.FruitHitGround -= RemoveHealth;

			startMenu.StartPressed -= StartGame;
			startMenu.ExitApplicationPressed -= ExitApplication;

			pauseMenu.ResumePressed -= Resume;
			pauseMenu.RestartPressed -= StartGame;
			pauseMenu.backToMenuPressed -= BackToMenu;

			gameOverMenu.RestartPressed -= StartGame;
			gameOverMenu.backToMenuPressed -= BackToMenu;
		}
	}

}
