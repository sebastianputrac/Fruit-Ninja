using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Core.Menu
{
	public class GameOverMenuController : MenuController
	{
		[SerializeField] private Button restartButton;
		[SerializeField] private Button backToMenuButton;

		public Action RestartPressed;
		public Action backToMenuPressed;

		private void Awake()
		{
			restartButton.onClick.AddListener(() => RestartPressed());
			backToMenuButton.onClick.AddListener(() => backToMenuPressed());
		}
		private void OnDestroy()
		{
			restartButton.onClick.RemoveListener(() => RestartPressed());
			backToMenuButton.onClick.RemoveListener(() => backToMenuPressed());
		}
	}
}

