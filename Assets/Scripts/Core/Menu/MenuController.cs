using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core.Menu
{
	public class MenuController : MonoBehaviour
	{
		[SerializeField] private GameObject canvas;
		public void Activate()
		{
			canvas.SetActive(true);
		}

		public void Deactivate()
		{
			canvas.SetActive(false);
		}
	}

}
