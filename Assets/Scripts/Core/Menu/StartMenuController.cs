using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Core.Menu
{
	public class StartMenuController : MenuController
{
		[SerializeField] private Button startButton;
		[SerializeField] private Button exitApplicationButton;

		public Action StartPressed;
		public Action ExitApplicationPressed;
		private void Awake()
		{
			startButton.onClick.AddListener(() => StartPressed());
			exitApplicationButton.onClick.AddListener(() => ExitApplicationPressed());
		}

		private void OnDestroy()
		{
			startButton.onClick.RemoveListener(() => StartPressed());
			exitApplicationButton.onClick.RemoveListener(() => ExitApplicationPressed());
		}
	}

}
