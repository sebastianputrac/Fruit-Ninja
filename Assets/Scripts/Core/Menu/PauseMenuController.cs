using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Core.Menu
{
	public class PauseMenuController : MenuController
	{
		[SerializeField] private Button resumeButton;
		[SerializeField] private Button restartButton;
		[SerializeField] private Button backToMenuButton;

		public Action ResumePressed;
		public Action RestartPressed;
		public Action backToMenuPressed;

		private void Awake()
		{
			resumeButton.onClick.AddListener(() => ResumePressed());
			restartButton.onClick.AddListener(() => RestartPressed());
			backToMenuButton.onClick.AddListener(() => backToMenuPressed());
		}
		private void OnDestroy()
		{
			resumeButton.onClick.RemoveListener(() => ResumePressed());
			restartButton.onClick.RemoveListener(() => RestartPressed());
			backToMenuButton.onClick.RemoveListener(() => backToMenuPressed());
		}
	}

}
