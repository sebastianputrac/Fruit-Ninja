using Core;
using TMPro;
using UnityEngine;

namespace GameUI
{
	public class GameUiController : MonoBehaviour
	{
		[SerializeField] private GameManager gameManager;
		[SerializeField] private TextMeshProUGUI pointsText;
		[SerializeField] private GameObject[] healthUI;

		private const int initialPointsValue = 0;
		private const int initialHealthValue = 3;

		private void Awake()
		{
			gameManager.PointGained += UpdatePointsUI;
			gameManager.HealthLost += UpdateHealthUI;
			gameManager.BombExploded += ClearHealthUI;

			gameManager.GameRestarted += ResetUI;
		}
		private void ResetUI()
		{
			UpdatePointsUI(initialPointsValue);
			UpdateHealthUI(initialHealthValue);
		}
		private void UpdatePointsUI(int points)
		{
			pointsText.text = "Points : " + points;
		}
		private void UpdateHealthUI(int health)
		{
			ClearHealthUI();
			for (int i = 0; i < health; i++)
			{
				healthUI[i].SetActive(true);
			}
		}
		private void ClearHealthUI()
		{
			for (int i = 0; i < healthUI.Length; i++)
			{
				healthUI[i].SetActive(false);
			}
		}
		private void OnDestroy()
		{
			gameManager.PointGained -= UpdatePointsUI;
			gameManager.HealthLost -= UpdateHealthUI;
			gameManager.BombExploded -= ClearHealthUI;

			gameManager.GameRestarted -= ResetUI;
		}
	}
}