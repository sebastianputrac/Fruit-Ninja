using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Core;

namespace Audio
{
	public class AudioController : MonoBehaviour
	{
		[SerializeField] private GameManager gameManager;
		[SerializeField] private AudioSource[] soundEffects;

		private void Awake()
		{
			gameManager.PointGained += PlayPointGainedSFX;
			gameManager.HealthLost += PlayHealthLostSFX;
			gameManager.BombExploded += PlayBombExplodedSFX;
		}
		private void PlayPointGainedSFX(int unused)
		{
			soundEffects[0].Play();
		}
		private void PlayHealthLostSFX(int unused)
		{
			soundEffects[1].Play();
		}
		private void PlayBombExplodedSFX()
		{
			soundEffects[2].Play();
		}
		private void OnDestroy()
		{
			gameManager.PointGained -= PlayPointGainedSFX;
			gameManager.HealthLost -= PlayHealthLostSFX;
			gameManager.BombExploded -= PlayBombExplodedSFX;
		}
	}
}

